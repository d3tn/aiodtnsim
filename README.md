# aiodtnsim

A minimal framework for performing DTN simulations based on Python 3.7 and asyncio.

Note that this project is still a **work in progress**.

## Requirements

* Python 3.7+
* NumPy
* tqdm for the progress bars
* [dtn-tvg-util](https://gitlab.com/d3tn/dtn-tvg-util)
* for uPCN integration, [uPCN](https://upcn.eu) v0.7.0+ with the `pyupcn` module installed in the Python environment

## Getting Started

Just install `aiodtnsim` via `pip`, e.g., in a virtual environment:

```
pip install aiodtnsim
```

For generating satellite scenarios (needed by the example script), you need to install the `dtn-tvg-util` Ring Road dependencies additionally:

```
pip install "dtn-tvg-util[ring_road]"
```

Now, you should be able to use the example scripts provided in the root directory of `aiodtnsim` to perform simple simulation runs, e.g. via:

```
bash examples/example_test_run.sh
```

## Development Setup

First, clone the `aiodtnsim`, `dtn-tvg-util`, and `upcn` repositories (the latter only for using uPCN emulation) and change into the `aiodtnsim` directory.
Now, create a virtual environment and install the required dependencies:

```
python3 -m venv --without-pip .venv
curl -sS https://bootstrap.pypa.io/get-pip.py | .venv/bin/python
source .venv/bin/activate
pip install -e .
pip install -e "../dtn-tvg-util[ring_road,gs_placement]"
pip install -U -r ../upcn/pyupcn/requirements.txt
python ../upcn/pyupcn/install.py
```

If you want to perform a run with uPCN, ensure the latest binary has been built:

```
cd ../upcn
make
```

## Concepts

This section aims to explain some basic core concepts of `aiodtnsim`. It is by no means exhaustive but should give you an idea of the assumptions underlying the implementation.

### Node Identifiers

In `aiodtnsim`, all network nodes are uniquely identified by a `node_id`. Each node has one, and only one, unique `node_id`. These identifiers are used as source and destination of exchanged bundles.

### Contacts

A contact is a time interval bounded by `start_time` and `end_time`, during which communication from a transmitting node (`tx_node`) to a receiving node (`rx_node`) is possible. Every contact is associated with a maximum transmission rate (`bit_rate`) and a one-way delay (`delay`). Furthermore, custom data for implementation-dependent use can be attached as `param`. These are not modified or evaluated by `aiodtnsim`.

### Bundles

The basic message type in `aiodtnsim` is called `Bundle` and corresponds to a [DTN bundle](https://tools.ietf.org/html/draft-ietf-dtn-bpbis).

A notable difference between the `aiodtnsim` `Bundle` type and the DTN bundle protocol is the use of node identifiers for sources _and destinations_. At the moment, `aiodtnsim` does not support advanced DTN addressing concepts including late binding. Thus, all endpoint identifiers (EID) have to be resolved to node identifiers when bundles enter `aiodtnsim`, e.g., in an emulation run. Support for EID handling and late binding is discussed to be included in a future version of `aiodtnsim`.

`Bundle` instances can be fragmented. The method `original_bundle` returns a `Bundle` instance mimicking (i.e., comparing equal to) the original un-fragmented `Bundle` instance. If a `Bundle` is not fragmented, the `fragment_offset` and `total_size` fields are `None`.

Similar to the DTN bundle protocol, `aiodtnsim` identifies bundles uniquely by their `start_time`, `source`, `fragment_offset`, and `size`. (The `size` is included as the concept of an increasing sequence number is missing.)

This way, any replication of bundles has to be performed upon transmission. A given bundle can only be part of a node's buffer once, even if different metadata are attached. It is suggested that changing metadata are attached only upon bundle transmission and detached upon reception.

### Units

`aiodtnsim` tries to be as unit-agnostic as possible to minimize necessary conversions and resulting issues. For example, in simulations it does not matter if bit rates are in bit per second or bytes per second, the only important thing is to specify bundle sizes with the same base unit.

However, it is **strongly suggested** to use the units stated in the documentation, e.g., bits and seconds. If conversions become necessary (e.g., for interaction with DTN software implementations in emulation runs), these units are applied.

### Transmission and Reception Channels

Conversely to simulators such as [the ONE](http://akeranen.github.io/the-one/), `aiodtnsim` makes no assumptions how many concurrent transfers are possible to or from a given node. This means that if there are concurrent incoming or outgoing contacts, bundles can be transferred concurrently as well.

In reality, there are always some limitations how many concurrent transfers can be accomplished, which depend on the used communication systems. `aiodtnsim` takes a simple approach to simulate such limitations based on `asyncio.BoundedSemaphore`: If a limited number of `tx_channels` or `rx_channels` are specified in the constructor of `BaseNode`, the standard `Link` implementation will allocate such a channel for each outgoing (in the case of `tx_channels`) or incoming (in the case of `rx_channels`) transfer. The transfer will block until both the transmitting node as well as the receiving node can allocate a channel.

### Simulation Monitoring and Statistic Generation

`aiodtnsim` supports a "hook-like" concept to decouple the simulator core from monitoring, logging, and reporting facilities. This is implemented in the classes `EventDispatcher` and `EventHandler`: Simulation nodes call methods of any attached `EventHandler` via the `EventDispatcher` to notify them about the simulation progress. `EventHandler` instances can be flexibly attached before or even during a simulation run. A couple of standard implementations, e.g., for logging and statistic generation, are provided in the `aiodtnsim.reports` module.

### Routing Algorithms, Bundle Scheduling, and Node Behavior

`aiodtnsim` aims to provide a framework for cleanly and easily implementing a simulator for your own needs. Thus, its core makes minimal assumptions about the processes of bundle and contact handling.

Every node implementation has to define only three `async` coroutines:
- `schedule_contact`: Called for scheduling a (future) contact. This coroutine should wait until the `start_time` of the specified `Contact`, perform all necessary transmissions in a loop until the `end_time` is reached, and terminate afterwards.
- `schedule_injection`: Called for scheduling the (future) injection of a `Bundle`. This coroutine should wait until the `start_time` of the given `Bundle`, inject it into the network at the node for which it was called, and terminate after all necessary processing of the `Bundle` has been performed.
- `schedule_reception`: Called for scheduling the (future) reception of a `Bundle`. This coroutine should wait until the specified `delay` has passed, process the specified `Bundle` for forwarding it further, and terminate after all necessary processing of the `Bundle` has been performed.

Due to this architecture, running "dry" simulations is possible as well as attaching actual DTN protocol implementations which require advance configuration of some means. Different types of nodes (simulated as well as emulated ones) can be even combined during the same run.

### The Simulation Event Loop

For running non-realtime simulations for arbitrary time periods as fast as possible, an alternative `asyncio.AbstractEventLoop` is provided in `aiodtnsim.simulation.event_loop`, based on [this Gist](https://gist.github.com/damonjw/35aac361ca5d313ee9bf79e00261f4ea), which is MIT-licensed.

This way, the same code can be used for simulating network scenarios as fast as possible and emulating them in realtime with DTN software implementations attached, just by exchanging the event loop implementation.

## License

`aiodtnsim` is provided under the MIT license. See [LICENSE](LICENSE) for details.

## Acknowledgments

The simulation event loop is based upon [code by Damon Wischik](https://gist.github.com/damonjw/35aac361ca5d313ee9bf79e00261f4ea).
