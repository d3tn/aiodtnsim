#!/usr/bin/env python
# encoding: utf-8

"""A minimal example script for running a simulation."""

import argparse
import json
import sys
import asyncio
import logging

from tvgutil import tvg

import aiodtnsim
import aiodtnsim.reports
import aiodtnsim.reports.logging
import aiodtnsim.simulation.epidemic_node
import aiodtnsim.simulation.event_loop


def _main(args):
    # Create and register event loop and logging facilities
    loop = aiodtnsim.simulation.event_loop.DESEventLoop(start_time=0)
    logger = _initialize_logger(args.verbose, loop)
    aiodtnsim.reports.logging.logger = logger
    asyncio.set_event_loop(loop)

    # Load TVGs and transmission plan
    ftvg = tvg.from_serializable(json.load(args.FTVGFILE))
    transmission_plan = json.load(args.TPLANFILE)

    # Initialize simulation monitoring/reporting
    event_dispatcher = aiodtnsim.EventDispatcher()
    event_dispatcher.add_subscriber(aiodtnsim.reports.logging.LoggingReport())
    # bp_report = aiodtnsim.reports.BundlePathReport()
    # event_dispatcher.add_subscriber(bp_report)
    bs_report = aiodtnsim.reports.BundleStatsReport()
    event_dispatcher.add_subscriber(bs_report)

    # Initialize nodes
    sim_nodes = {
        node_id: aiodtnsim.simulation.epidemic_node.EpidemicNode(
            node_id,
            args.buffersize,
            event_dispatcher,
        )
        for node_id in ftvg.vertices
    }

    # Initialize and schedule contacts
    for _, contact_list in ftvg.edges.items():
        for factual_contact in contact_list:
            sim_contact = aiodtnsim.Contact(
                tx_node=sim_nodes[factual_contact.tx_node],
                rx_node=sim_nodes[factual_contact.rx_node],
                start_time=factual_contact.start_time,
                end_time=factual_contact.end_time,
                bit_rate=factual_contact.to_simple().bit_rate,
                delay=factual_contact.to_simple().delay,
                param=factual_contact,
            )
            asyncio.ensure_future(
                sim_contact.tx_node.schedule_contact(sim_contact)
            )

    # Schedule Bundles
    for message_parameters in transmission_plan:
        # message_parameters is a list with the same first elements
        bundle = aiodtnsim.Bundle(*message_parameters)
        assert bundle.deadline > bundle.start_time
        asyncio.ensure_future(
            sim_nodes[bundle.source].schedule_injection(bundle)
        )

    # Run simulation by firing up the event loop
    loop.run_forever()

    # Print report results
    bs_report.print()
    if args.savestats:
        json.dump(
            bs_report.serializable(),
            args.savestats,
            indent=(4 if args.indent else None),
        )
        args.savestats.write("\n")
        args.savestats.close()


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "FTVGFILE",
        type=argparse.FileType("r"),
        help="the factual TVG (F-TVG) JSON file",
    )
    parser.add_argument(
        "TPLANFILE",
        type=argparse.FileType("r"),
        help="the transmission plan JSON file",
    )
    parser.add_argument(
        "-s", "--savestats",
        type=argparse.FileType("w"),
        default=None,
        help="a file used for saving bundle stats",
    )
    parser.add_argument(
        "-i", "--indent",
        action="store_true",
        help="indent the JSON output for bundle stats",
    )
    parser.add_argument(
        "-v", "--verbose",
        action="count",
        default=0,
        help="increase output verbosity",
    )
    parser.add_argument(
        "-b", "--buffersize",
        type=float, default=-1,
        help="the node buffer size, in bit (default: unlimited)",
    )
    return parser


def _initialize_logger(verbosity: int, event_loop: asyncio.AbstractEventLoop):

    def _add_time_to_record(record):
        record.sim_time = asyncio.get_running_loop().time()
        return True

    logging.basicConfig(
        level={
            0: logging.WARN,
            1: logging.INFO,
        }.get(verbosity, logging.DEBUG),
        format="%(sim_time).2f %(levelname)s: %(message)s",
    )
    logger = logging.getLogger(sys.argv[0])
    logger.addFilter(_add_time_to_record)
    return logger


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
