#!/usr/bin/env python
# encoding: utf-8

"""Script to run an emulation using the uPCN DTN software."""

import argparse
import json
import sys
import asyncio
import logging
import math

import tvgutil.contact_plan
import tvgutil.tvg

import aiodtnsim
import aiodtnsim.reports
import aiodtnsim.reports.logging
import aiodtnsim.timeutil
import aiodtnsim.emulation.upcn_node


def _main(args):
    # Create and register event loop and logging facilities
    loop = asyncio.get_event_loop()
    logger = _initialize_logger(args.verbose, loop)
    aiodtnsim.reports.logging.logger = logger

    # Load TVGs and transmission plan
    ftvg = tvgutil.tvg.from_serializable(json.load(args.FTVGFILE))
    transmission_plan = json.load(args.tplan) if args.tplan else []

    # Initialize simulation monitoring/reporting
    event_dispatcher = aiodtnsim.EventDispatcher()
    event_dispatcher.add_subscriber(aiodtnsim.reports.logging.LoggingReport())
    bs_report = aiodtnsim.reports.BundleStatsReport()
    event_dispatcher.add_subscriber(bs_report)

    # Initialize nodes
    sim_nodes = {
        node_id: aiodtnsim.emulation.upcn_node.UPCNNode(
            node_id=node_id,
            event_dispatcher=event_dispatcher,
            upcn_path=args.upcn_path,
            spp_port=(args.spp_port_offset + i),
            aap_port=(args.aap_port_offset + i),
            reachable_nodes=ftvg.vertices,
            contact_setup_time=args.contact_setup_time,
            contact_min_duration=args.contact_min_duration,
            max_tx_channels=args.max_simultaneous_contacts,
            max_rx_channels=args.max_simultaneous_contacts,
            aap_lifetime=int(args.aap_lifetime / args.acceleration_factor),
        )
        for i, node_id in enumerate(ftvg.vertices)
    }

    time_offset = loop.time() + args.init_offset

    def tosimtime(tval):
        # Converts the time offset to a simulation (accelerated) timestamp.
        # int conversion is important to keep Bundles searchable
        return int(tval / args.acceleration_factor + time_offset)

    max_end = _schedule_contacts(
        sim_nodes,
        ftvg,
        tosimtime,
        args.acceleration_factor,
    )
    _schedule_bundles(
        sim_nodes,
        transmission_plan,
        tosimtime,
    )

    # Run simulation by firing up the event loop
    sim_duration = int(math.ceil(max_end - loop.time()))
    print("aiodtnsim :: Firing up event loop!")
    print(f"This emulation run will take {sim_duration} seconds.")
    assert sim_duration < 86400, "a runtime over 24h is not supported"

    try:
        loop.run_until_complete(asyncio.sleep(sim_duration))
        # loop.run_until_complete(loop.shutdown_asyncgens())
        # loop.close()
    finally:
        # Print report results
        bs_report.print()
        if args.savestats:
            json.dump(
                bs_report.serializable(),
                args.savestats,
                indent=(4 if args.indent else None),
            )
            args.savestats.write("\n")
            args.savestats.close()


def _schedule_contacts(sim_nodes, ftvg, tosimtime, acceleration_factor):
    # Schedules the provided FTVG as simulation contacts.
    # Returns the end time of the simulation run.
    max_end = -1
    for _, contact_list in ftvg.edges.items():
        for factual_contact in contact_list:
            start_time = tosimtime(factual_contact.start_time)
            end_time = tosimtime(factual_contact.end_time)
            tx_rate = int(
                factual_contact.to_simple().bit_rate * acceleration_factor
            )
            assert start_time > asyncio.get_event_loop().time()
            max_end = max(max_end, end_time)
            sim_contact = aiodtnsim.Contact(
                tx_node=sim_nodes[factual_contact.tx_node],
                rx_node=sim_nodes[factual_contact.rx_node],
                start_time=start_time,
                end_time=end_time,
                bit_rate=tx_rate,
                delay=0,
            )
            asyncio.ensure_future(
                sim_contact.tx_node.schedule_contact(sim_contact)
            )
    return max_end


def _schedule_bundles(sim_nodes, transmission_plan, tosimtime):
    for message_parameters in transmission_plan:
        start_time, source, dest, size, deadline = message_parameters
        start_time = tosimtime(start_time)
        deadline = tosimtime(deadline)
        bundle = aiodtnsim.Bundle(start_time, source, dest, size, deadline)
        assert bundle.deadline > bundle.start_time
        asyncio.ensure_future(
            sim_nodes[bundle.source].schedule_injection(bundle)
        )


def _get_argument_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument(
        "FTVGFILE",
        type=argparse.FileType("r"),
        help="the factual TVG (F-TVG) JSON file",
    )
    parser.add_argument(
        "-t", "--tplan",
        type=argparse.FileType("r"), default=None,
        help="the transmission plan JSON file",
    )
    parser.add_argument(
        "-s", "--savestats",
        type=argparse.FileType("w"),
        default=None,
        help="a file used for saving bundle stats",
    )
    parser.add_argument(
        "-a", "--acceleration-factor",
        type=int, default=100,
        help=("the factor by which the time should be accelerated "
              "(default: 100, 1 equals a real-time simulation)"),
    )
    parser.add_argument(
        "--upcn-path",
        default="../upcn/build/posix/upcn",
        help="the path to the uPCN binary",
    )
    parser.add_argument(
        "--spp-port-offset",
        type=int, default=20040,
        help="the first port number used for SPP",
    )
    parser.add_argument(
        "--aap-port-offset",
        type=int, default=30040,
        help="the first port number used for AAP",
    )
    parser.add_argument(
        "--init-offset",
        type=float, default=5,
        help=(
            "the amount of seconds allowed to initialize the simulator "
            "(default = 5 s)"
        ),
    )
    parser.add_argument(
        "--contact-setup-time",
        type=float, default=1,
        help=(
            "the amount of seconds a contact in the simulator will be "
            "initialized earlier than scheduled in the PCP (default = 1 s)"
        ),
    )
    parser.add_argument(
        "--contact-min-duration",
        type=int, default=1,
        help=(
            "the minimum duration, in seconds, of a contact in uPCN "
            "(default = 1 s)"
        ),
    )
    parser.add_argument(
        "--max-simultaneous-contacts",
        type=int, default=None,
        help="the maximum amount of simultaneous contacts (default = inf)",
    )
    parser.add_argument(
        "--aap-lifetime",
        type=int, default=86400,
        help=(
            "the lifetime of bundles injected via AAP, in seconds "
            "(default = 86400 s = 1 day)"
        ),
    )
    parser.add_argument(
        "-i", "--indent",
        action="store_true",
        help="indent the JSON output for bundle stats",
    )
    parser.add_argument(
        "-v", "--verbose",
        action="count",
        default=0,
        help="increase output verbosity",
    )
    return parser


def _initialize_logger(verbosity: int, event_loop: asyncio.AbstractEventLoop):

    logging.basicConfig(
        level={
            0: logging.WARN,
            1: logging.INFO,
        }.get(verbosity, logging.DEBUG),
        format="%(asctime)s %(levelname)s: %(message)s",
    )
    logger = logging.getLogger(sys.argv[0])
    return logger


if __name__ == "__main__":
    _main(_get_argument_parser().parse_args())
