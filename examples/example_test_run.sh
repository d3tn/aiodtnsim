#!/bin/bash

set -o errexit

python3 -m tvgutil.tools.create_rr_scenario --gs 5 --sats 5 --hotspots 2 --output 01_scenario.json
python3 -m tvgutil.tools.create_rr_tvg --rr is --duration 86400 --minelev 10 --islrange 1000 --uplinkrate 9600 --downlinkrate 9600 --output 02_ptvg.json 01_scenario.json
python3 -m tvgutil.tools.convert_ptvg_to_ftvg --probabilistic --output 03_ftvg.json 02_ptvg.json
python3 -m tvgutil.tools.create_transmission_plan --interval 20 --lifetime 86400 --intervaldev 10 --maxgenpercent 67 --output 04_tplan.json 02_ptvg.json
python3 simulation.py --algo epidemic --progress --tplan 04_tplan.json 03_ftvg.json
python3 simulation.py --algo cgr --progress --tplan 04_tplan.json --ptvg 02_ptvg.json 03_ftvg.json
