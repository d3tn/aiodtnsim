# encoding: utf-8

"""Module providing an EventHandler for generating comparable logs."""

import sys

from .. import EventHandler


class BundleOpsReport(EventHandler):
    """An report providing a diff-able list of the most relevant events."""

    def __init__(self):
        self.ops = []
        self.bdls = {}  # ids
        self.next_bdl_id = 0

    def _get_id(self, bundle):
        return self.bdls[bundle]

    def contact_started(self, time, contact):
        """Add a contact_started event to the list of events."""
        self.ops.append(
            f"{_time(time)} {contact.tx_node.node_id} "
            f"c_start {contact.rx_node.node_id}"
        )

    def contact_ended(self, time, contact):
        """Add a contact_ended event to the list of events."""
        self.ops.append(
            f"{_time(time)} {contact.tx_node.node_id} "
            f"c_end {contact.rx_node.node_id}"
        )

    def bundle_received(self, time, rx_node, bundle, tx_node):
        """Add a bundle_received event to the list of events."""
        if bundle not in self.bdls:
            return
        self.ops.append(
            f"{_time(time)} {rx_node.node_id} recv {self._get_id(bundle)} "
            f"from {tx_node.node_id}"
        )

    def bundle_delivered(self, time, node, bundle):
        """Add a bundle_delivered event to the list of events."""
        self.ops.append(
            f"{_time(time)} {node.node_id} deliver {self._get_id(bundle)}"
        )

    def bundle_rejected(self, time, node, bundle):
        """Add a bundle_rejected event to the list of events."""
        self.ops.append(
            f"{_time(time)} {node.node_id} reject {self._get_id(bundle)}"
        )

    def bundle_dropped(self, time, node, bundle):
        """Add a bundle_dropped event to the list of events."""
        self.ops.append(
            f"{_time(time)} {node.node_id} drop {self._get_id(bundle)}"
        )

    def bundle_transmission_aborted(self, time, bundle, contact):
        """Add a bundle_transmission_aborted event to the list of events."""
        if bundle not in self.bdls:
            return
        self.ops.append(
            f"{_time(time)} {contact.tx_node.node_id} "
            f"abort {self._get_id(bundle)}"
        )

    def bundle_injected(self, time, node, bundle):
        """Add a bundle_injected event to the list of events."""
        self.bdls[bundle] = self.next_bdl_id
        self.next_bdl_id += 1
        self.ops.append(
            f"{_time(time)} {node.node_id} inject {self._get_id(bundle)} "
            f"({bundle.source} -> {bundle.destination})"
        )

    def print(self, file=sys.stdout):
        """Print the report to the specified file. Defaults to stdout."""
        for operation in sorted(self.ops):
            print(operation, file=file)


def _time(time):
    return round(time * 1000)
