# encoding: utf-8

"""Module providing an EventHandler for generating log output."""

import logging

from .. import EventHandler

logger = logging.getLogger(__name__)


class LoggingReport(EventHandler):
    """A simple event handler, logging all events that occur."""

    def contact_started(self, time, contact):
        """Log a contact_started event."""
        logger.debug("Contact started: %s", contact)

    def contact_ended(self, time, contact):
        """Log a contact_ended event."""
        logger.debug("Contact ended: %s", contact)

    def bundle_transmission_started(self, time, bundle, contact):
        """Log a bundle_transmission_started event."""
        logger.debug("Bundle tx started via %s: %s", contact, bundle)

    def bundle_received(self, time, rx_node, bundle, tx_node):
        """Log a bundle_received event."""
        logger.debug("Bundle received at %s from %s: %s",
                     rx_node.node_id, tx_node.node_id, bundle)

    def bundle_dropped(self, time, node, bundle):
        """Log a bundle_dropped event."""
        logger.debug("Bundle dropped at %s: %s", node.node_id, bundle)

    def bundle_rejected(self, time, node, bundle):
        """Log a bundle_rejected event."""
        logger.debug("Bundle rejected at %s: %s", node.node_id, bundle)

    def bundle_delivered(self, time, node, bundle):
        """Log a bundle_delivered event."""
        logger.debug("Bundle delivered at %s: %s", node.node_id, bundle)

    def bundle_deleted(self, time, node, bundle):
        """Log a bundle_deleted event."""
        logger.debug("Bundle deleted at %s: %s", node.node_id, bundle)

    def bundle_injected(self, time, node, bundle):
        """Log a bundle_injected event."""
        logger.debug("Bundle injected at %s: %s", node.node_id, bundle)

    def bundle_scheduled(self, time, node, bundle):
        """Log a bundle_scheduled event."""
        logger.debug("Bundle scheduled at %s: %s", node.node_id, bundle)

    def bundle_transmission_completed(self, time, bundle, contact):
        """Log a bundle_transmission_completed event."""
        logger.debug("Sent via %s: %s", contact, bundle)

    def bundle_transmission_aborted(self, time, bundle, contact):
        """Log a bundle_transmission_aborted event."""
        logger.debug("Transfer via %s aborted: %s", contact, bundle)
