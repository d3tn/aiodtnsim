# encoding: utf-8

"""Module providing a Node that simulates ONE Epidemic Routing."""

import random

from .opportunistic_node import DropStrategy, OpportunisticNode


class ONEEpidemicNode(OpportunisticNode):
    """A Node using a variant of Epidemic Routing as implemented in ONE.

    Compared to Epidemic Routing, this approach does NOT exchange Bundle
    vectors. It first delivers Bundles for which the next node is the
    destination. Secondly, random Bundles are sent, but only if the
    destination does not have them which is checked without delay.

    Args:
        node_id: A unique identifier for the node.
        buffer_size: The amount of data (in bits) the node's buffer can hold.
        event_dispatcher: An EventDispatcher for monitoring the simulation.
        link_factory: A callable returning a ``Link`` instance for the given
            parameters. If not provided, ``Link`` will be used.

    """

    def __init__(self, node_id, buffer_size, event_dispatcher, **kwargs):
        super().__init__(
            node_id,
            buffer_size,
            event_dispatcher,
            drop_strategy=DropStrategy.FIFO,  # first-received first
            **kwargs
        )
        self.delivered = set()

    async def get_bundles(self, rx_node):
        """Asynchronously yield Bundles to be transmitted."""
        in_flight = None
        while True:
            population = (
                self.buf.bundles -
                rx_node.buf.bundles -
                rx_node.delivered
            )
            bundle = None
            for candidate in random.sample(population, len(population)):
                if candidate is in_flight:
                    continue
                if bundle is None:
                    # assume first Bundle neigh does not have if no candidate
                    # continue to search for "deliverable"
                    bundle = candidate
                if rx_node.node_id == candidate.destination:
                    # if there is a "deliverable" Bundle, use it directly
                    bundle = candidate
                    break
            # if nothing found, wait until a new Bundle arrives
            if bundle is None:
                in_flight = None
                await self.wait_until_new_bundle_scheduled()
                continue
            in_flight = bundle
            yield bundle, None

    def route(self, bundle, tx_node, metadata):
        """Schedule the provided Bundle for reception at the next hop."""
        if self.node_id == bundle.destination:
            self.delivered.add(bundle)
        super().route(bundle, tx_node, metadata)
