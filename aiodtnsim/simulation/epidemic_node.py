# encoding: utf-8

"""Module providing a Node that uses Epidemic Routing."""

import asyncio
import random

from aiodtnsim import Bundle

from .opportunistic_node import OpportunisticNode

# Amount of seconds assumed for the TTL of the summary vector Bundle.
VECTOR_BUNDLE_LIFETIME = 300


class SummaryVectorBundle(Bundle):
    """A Bundle encapsulating Epidemic Routing summary vectors."""

    pass


class EpidemicNode(OpportunisticNode):
    """A node using Epidemic Routing.

    A gratuitous (proactive) acknowledgment mechanism is leveraged by this
    implementation to exchange the "summary vectors" at the beginning of a
    contact, i.e. inform the other node(s) which Bundle(s) we have and only
    transmit those that the other node(s) don't have.

    Args:
        node_id: A unique identifier for the node.
        buffer_size: The amount of data (in bits) the node's buffer can hold.
        event_dispatcher: An EventDispatcher for monitoring the simulation.
        link_factory: A callable returning a ``Link`` instance for the given
            parameters. If not provided, ``Link`` will be used.
        drop_strategy: The dropping behavior if a new Bundle is received.
        vector_entry_size (int): The size of a bdl. vector entry (bits).
        vector_header_size (int): The amount of bits assumed as header overhead
            included in the "summary vector" Bundle.
        max_hops (int): The maximum number of hops (transmissions since
            creation, excluding replicas) allowed for a Bundle.
        deliverable_first (bool): Whether to send Bundles addressed to the
            encountered neighbor first (may improve delays, used in ONE).
        send_once (bool): Send Bundles only once during a contact.

    """

    def __init__(self, node_id, buffer_size, event_dispatcher,
                 vector_entry_size=32, vector_header_size=800, max_hops=6,
                 deliverable_first=True, send_once=True, **kwargs):
        super().__init__(
            node_id,
            buffer_size,
            event_dispatcher,
            **kwargs
        )
        self.vector_entry_size = vector_entry_size
        self.vector_header_size = vector_header_size
        self.max_hops = max_hops
        self.deliverable_first = deliverable_first
        self.send_once = send_once
        self.last_vector = {}
        self.delivered = set()
        self._bdl_remaining_hops = {}

    async def get_bundles(self, rx_node):
        """Asynchronously yield Bundles to be transmitted."""
        if rx_node not in self.last_vector:
            self.last_vector[rx_node] = set()
        # First, send the "summary vector".
        time = asyncio.get_running_loop().time()
        vector_contents = self.buf.bundles | self.delivered
        yield SummaryVectorBundle(
            start_time=time,
            source=self.node_id,
            destination=rx_node.node_id,
            size=(
                self.vector_entry_size * len(vector_contents) +
                self.vector_header_size
            ),
            deadline=(
                time + VECTOR_BUNDLE_LIFETIME
            ),
            data=vector_contents,
        ), None
        # Now, send the Bundles not known to be available at the neighbor.
        # we run in an endless loop - even if no Bundles can be sent
        # anymore, we await the next reception event via the lock
        sent = set()
        while True:
            last_vector = self.last_vector[rx_node]
            if self.deliverable_first:
                population = [
                    bdl for bdl in self.buf.bundles
                    if bdl not in last_vector
                    and self._bdl_remaining_hops[bdl] > 1
                    and rx_node.node_id != bdl.destination
                    and bdl not in sent
                ]
                deliverable = [
                    bdl for bdl in self.buf.bundles
                    if bdl not in last_vector
                    and rx_node.node_id == bdl.destination
                    and bdl not in sent
                ]
            else:
                population = [
                    bdl for bdl in self.buf.bundles
                    if bdl not in last_vector
                    and (
                        self._bdl_remaining_hops[bdl] > 1
                        or rx_node.node_id == bdl.destination
                    )
                    and bdl not in sent
                ]
                deliverable = []
            random.shuffle(population)
            random.shuffle(deliverable)
            # We check for the _current_ Event being set and keep a reference
            # because ScheulingEventNode replaces it upon Bundle scheduling.
            # By that, the loop would effectively be endless if using
            # self.get_event().is_set().
            # NOTE that this allows sending Bundles just received from the
            # neighbor which is, however, prevented by sending only once
            # during a contact and exchanging new summary vectors at the
            # start of the next contact.
            event = self.get_event()
            while not event.is_set():
                if deliverable:
                    # First, send Bundles for which neighbor == receiver.
                    # This behavior has been adopted from the ONE simulator.
                    used_list = deliverable
                elif population:
                    # Second, send other Bundles for which we have copies.
                    used_list = population
                else:
                    # Nothing to send, wait for a new Bundle.
                    await self.wait_until_new_bundle_scheduled()
                    break
                # Send ONE Bundle.
                bundle = used_list.pop()
                # It might happen that a Bundle is removed from the buffer
                # but nothing new gets scheduled, so we don't need a new
                # population but have to check whether we still have it.
                if not self.buf.contains(bundle):
                    continue
                if bundle.deadline < asyncio.get_running_loop().time():
                    self.event_dispatcher.bundle_dropped(self, bundle)
                    self.buf.remove(bundle)
                    continue
                # Yield the Bundle.
                yield bundle, self._bdl_remaining_hops[bundle] - 1
                # If the Bundle was not sent out successfully, we will not
                # get here but yield will throw.
                if self.send_once:
                    sent.add(bundle)

    async def schedule_reception(self, bundle, tx_node, delay, metadata):
        """Schedule the provided Bundle for reception at the local node."""
        if isinstance(bundle, SummaryVectorBundle):
            await asyncio.sleep(delay)
            self.last_vector[tx_node] = bundle.data
            # Trigger an update of the list of Bundles to be sent or unblock
            # get_bundles as the summary vector has been updated.
            self.set_bundle_scheduled()
        else:
            await super().schedule_reception(bundle, tx_node, delay, metadata)

    def route(self, bundle, tx_node, metadata):
        """Schedule the provided Bundle for reception at the next hop."""
        if self.node_id == bundle.destination:
            self.delivered.add(bundle)
        if tx_node:
            if (bundle not in self._bdl_remaining_hops or
                    metadata > self._bdl_remaining_hops.get(bundle, 1)):
                self._bdl_remaining_hops[bundle] = metadata  # received
        else:
            self._bdl_remaining_hops[bundle] = self.max_hops  # injected
        super().route(bundle, tx_node, metadata)
