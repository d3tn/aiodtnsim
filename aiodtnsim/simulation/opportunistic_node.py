# encoding: utf-8

"""Module providing a Node that allows configurable Bundle dropping."""

import asyncio
import enum
import random

from .scheduling_event_node import SchedulingEventNode


class DropStrategy(enum.Enum):
    """Definitions for strategies concerning Bundle dropping."""

    EXPIRED = "drop only expired Bundles"
    EDF = "drop Bundles with earliest deadline first"
    LDF = "drop Bundles with latest deadline first"
    FIFO = "track and drop first-received Bundles first"
    RANDOM = "drop random Bundles"


class OpportunisticNode(SchedulingEventNode):
    """A Node behaving like ActiveRouter in ONE.

    This node drops Bundles by first reception time as done in the ONE.

    Args:
        node_id: A unique identifier for the node.
        buffer_size: The amount of data (in bits) the node's buffer can hold.
        event_dispatcher: An EventDispatcher for monitoring the simulation.
        link_factory: A callable returning a ``Link`` instance for the given
            parameters. If not provided, ``Link`` will be used.
        drop_strategy: The dropping behavior if a new Bundle is received.

    """

    def __init__(self, node_id, buffer_size, event_dispatcher,
                 drop_strategy=DropStrategy.FIFO, **kwargs):
        super().__init__(
            node_id,
            buffer_size,
            event_dispatcher,
            **kwargs
        )
        self.drop_strategy = drop_strategy
        self._reception_times = {}

    def drop_bundles(self, incoming_bundle, tx_node):
        """Drop Bundles from buffer to make space for a new Bundle."""
        # Perform configured default dropping by expiration time.
        super().drop_bundles(incoming_bundle, tx_node)
        if self.drop_strategy is DropStrategy.EXPIRED:
            return
        # Drop further Bundles by reception time.
        while self.buf.free < incoming_bundle.size and self.buf:
            if self.drop_strategy is DropStrategy.FIFO:
                # Get the Bundle with smallest (oldest) reception time.
                bdl = min(self.buf, key=lambda m: self._reception_times[m])
            if self.drop_strategy is DropStrategy.RANDOM:
                bdl = random.choice(self.buf)
            elif self.drop_strategy is DropStrategy.EDF:
                bdl = self.buf[0]
            elif self.drop_strategy is DropStrategy.LDF:
                bdl = self.buf[-1]
            # Drop it.
            self.buf.remove(bdl)
            self.event_dispatcher.bundle_dropped(self, bdl)

    def store_and_schedule(self, bundle, tx_node):
        """Add the provided Bundle to the buffer and schedule it."""
        super().store_and_schedule(bundle, tx_node)
        # Update stored timestamp to latest reception time.
        if self.drop_strategy is DropStrategy.FIFO and bundle in self.buf:
            self._reception_times[bundle] = asyncio.get_running_loop().time()
