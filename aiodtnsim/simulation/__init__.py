# encoding: utf-8

"""A Node implementation for simulating routing algorithms."""

from typing import (
    Any,
    Optional,
    Callable,
    Set,
    List,
    AsyncGenerator,
    Tuple,
)

import asyncio
import collections.abc
import dataclasses
import heapq

from .. import BaseNode, Contact, Bundle
from ..timeutil import duration_until, sleep_until


# Helpers for typing
LinkFactory = Callable[[Contact], "Link"]
BundleGenerator = AsyncGenerator[Tuple[Bundle, Any], None]


class SimNode(BaseNode):
    """Base class representing a simulated node.

    For own routing algorithms, at least the ``get_bundles`` asynchronous
    generator has to be implemented.

    Args:
        node_id: A unique identifier for the node.
        buffer_size: The amount of data (in bits) the node's buffer can hold.
        event_dispatcher: An EventDispatcher for monitoring the simulation.
        link_factory: A callable returning a ``Link`` instance for the given
            parameters. If not provided, ``Link`` will be used.

    """

    def __init__(self, node_id: str, buffer_size: float, event_dispatcher: Any,
                 link_factory: Optional[LinkFactory] = None, **kwargs) -> None:
        super().__init__(node_id, event_dispatcher, **kwargs)
        self.node_id = node_id
        self.buf = Buffer(buffer_size)
        self.event_dispatcher = event_dispatcher
        self.link_factory: LinkFactory = link_factory or Link

    async def schedule_contact(self, contact: Contact) -> None:
        """Schedule the provided contact for execution.

        This coroutine terminates after the link has been deestablished.

        """
        assert self is contact.tx_node
        link = self.link_factory(contact)
        await sleep_until(contact.start_time)
        self.event_dispatcher.contact_started(contact)
        try:
            await asyncio.wait_for(
                self._bundle_transmission(link, contact),
                timeout=duration_until(contact.end_time),
            )
        except asyncio.TimeoutError:
            pass
        else:
            await sleep_until(contact.end_time)
        self.event_dispatcher.contact_ended(contact)

    async def _bundle_transmission(self, link: "Link",
                                   contact: Contact) -> None:
        await link.establish()
        try:
            await link.transmit_bundles(self.get_bundles(contact.rx_node))
        finally:
            await link.teardown()

    async def schedule_injection(self, bundle: Bundle) -> None:
        """Schedule the provided Bundle for injection into the network.

        This coroutine terminates after the Bundle has been routed.

        """
        await sleep_until(bundle.start_time)
        self.event_dispatcher.bundle_injected(self, bundle)
        self.route(bundle, None, None)

    async def schedule_reception(self, bundle: Bundle, tx_node: BaseNode,
                                 delay: float, metadata: Any) -> None:
        """Schedule the provided Bundle for reception at the next hop.

        This coroutine terminates after the Bundle has been routed.

        """
        await asyncio.sleep(delay)
        self.event_dispatcher.bundle_received(self, bundle, tx_node)
        self.route(bundle, tx_node, metadata)

    def route(self, bundle: Bundle, tx_node: Optional[BaseNode],
              metadata: Any) -> None:
        """Evaluate how a given Bundle should be forwarded further.

        After obtaining a routing and/or dropping decision, the Bundle should
        be stored in the local buffer, if possible and applicable.

        Args:
            bundle: The Bundle to be routed.
            tx_node: The transmitting Node, or None if the Bundle has been
                injected locally.
            metadata: A parameter forwarded from the result of get_bundles().

        """
        # If we receive the Bundle after the deadline, it became useless...
        if bundle.deadline < asyncio.get_running_loop().time():
            self.event_dispatcher.bundle_dropped(self, bundle)
            return
        # By default Bundles are only buffered if they will be sent further.
        if bundle.destination == self.node_id:
            self.event_dispatcher.bundle_delivered(self, bundle)
            return
        # If the Bundle is too big or we have it already, reject it.
        if bundle.size > self.buf.size or self.buf.contains(bundle):
            self.reject_bundle(bundle, tx_node, metadata)
            return
        # Check what we can drop, and drop it.
        self.drop_bundles(bundle, tx_node)
        # If the Bundle still cannot fit in the buffer, reject it.
        if bundle.size > self.buf.free:
            self.reject_bundle(bundle, tx_node, metadata)
            return
        # Else, store and schedule the Bundle.
        self.store_and_schedule(bundle, tx_node)

    def drop_bundles(self, incoming_bundle: Bundle,
                     tx_node: Optional[BaseNode]) -> None:
        """Drop Bundles from buffer to make space for a new Bundle.

        This function checks the local buffer for Bundles which should be
        dropped after the reception or injection of another Bundle.

        Args:
            incoming_bundle: The Bundle leading to the invocation.
            tx_node: The transmitting Node, or None if the Bundle has been
                injected locally.

        """
        time = asyncio.get_running_loop().time()
        # Drop expired Bundles (w.r.t. their deadline).
        while True:
            candidate = self.buf.peek_edf()
            if not candidate or candidate.deadline >= time:
                break
            self.buf.pop_edf()
            self.event_dispatcher.bundle_dropped(self, candidate)

    def reject_bundle(self, bundle: Bundle, tx_node: Optional[BaseNode],
                      metadata: Any) -> None:
        """Reject the specified Bundle, e.g. because the buffer is exhausted.

        Args:
            bundle: The Bundle to be rejected.
            tx_node: The transmitting Node, or None if the Bundle has been
                injected locally.
            metadata: A parameter forwarded from the result of get_bundles().

        """
        self.event_dispatcher.bundle_rejected(self, bundle)

    def store_and_schedule(self, bundle: Bundle,
                           tx_node: Optional[BaseNode]) -> None:
        """Add the provided Bundle to the buffer and schedule it.

        After a positive routing decision has been obtained (in ``route``),
        this method stores the Bundle in the local buffer and schedules it
        for further forwarding.

        Args:
            bundle: The Bundle to be stored.
            tx_node: The transmitting Node, or None if the Bundle has been
                injected locally.

        """
        self.buf.add(bundle)
        self.event_dispatcher.bundle_scheduled(self, bundle)

    async def get_bundles(self, rx_node: BaseNode) -> BundleGenerator:
        """Asynchronously yield Bundles to be transmitted.

        The created generator object asynchronously yields all Bundles to be
        transmitted to the provided node. It should be noted that it is
        possible and expected for this function to wait until new Bundles
        have been received. Multiple contacts may occur concurrently and lead
        to situations where Bundles are received during the contact over which
        they should be sent further. This method may consider the current
        simulation time as provided by the running event loop.
        If the communication channel closes, a GeneratorExit exception is
        thrown into the async generator.

        Args:
            rx_node: The next hop for the Bundles yielded by the generator.

        """
        raise NotImplementedError()
        yield  # mark function as generator -> pylint: disable=unreachable

    def __repr__(self) -> str:
        """Get a string representation of the class instance."""
        return f"<Node node_id='{self.node_id}'>"


@dataclasses.dataclass(eq=False)
class Link:
    """Represents a unidirectional link between two nodes.

    By default, this class provides a simulated link which does not
    actually transmit data but waits for a calculated "transmission time"
    based on the specified bit rate and delay.

    Args:
        contact: The factual contact for which the link is established.

    """

    contact: Contact

    async def establish(self) -> None:
        """Establish the connection.

        This coroutine is called at the start of the underlying contact.
        By default, a virtual link is used which does not need to be
        established first.

        """

    async def transmit_bundles(self, generator: BundleGenerator) -> None:
        """Transmit the Bundles yielded by the provided generator.

        All Bundles the asyncronous generator yields are transmitted in
        order. It is expected that this coroutine is cancelled when the link
        is torn down.

        Args:
            generator: An asynchronous generator object yielding Bundles to
                be transmitted over the link.

        """
        contact = self.contact
        event_dispatcher = contact.tx_node.event_dispatcher
        async for bundle, metadata in generator:
            async with contact.tx_node.channel(contact.rx_node):
                try:
                    event_dispatcher.bundle_transmission_started(
                        bundle,
                        contact,
                    )
                    await self.transmit(bundle, metadata)
                except asyncio.CancelledError:
                    event_dispatcher.bundle_transmission_aborted(
                        bundle,
                        contact,
                    )
                    await generator.aclose()
                    raise
                else:
                    event_dispatcher.bundle_transmission_completed(
                        bundle,
                        contact,
                    )

    async def transmit(self, bundle: Bundle, metadata: Any) -> None:
        """Transmit the provided Bundle over the link.

        By default, a virtual link is used which schedules a reception event
        at the next hop with the configured transmission delay after sleeping
        for the calculated transmit duration.

        """
        # If the contact does not transmit anything, block indefinitely.
        if self.contact.bit_rate <= 0:
            while True:
                await asyncio.sleep(86400)  # duration should not exceed 1 day
        tx_duration = bundle.size / self.contact.bit_rate
        await asyncio.sleep(tx_duration)
        # We schedule reception of the Bundle after the specified link delay.
        asyncio.ensure_future(self.contact.rx_node.schedule_reception(
            bundle,
            self.contact.tx_node,
            self.contact.delay,
            metadata,
        ))

    async def teardown(self) -> None:
        """Close the connection and clean up afterwards.

        This coroutine is called at the end of the underlying contact.
        By default, a virtual link is used which does not need to be torn down.

        """


class Buffer(collections.abc.Sequence):
    """Represents a node buffer, allowing to store a specific amount of data.

    Args:
        size: The size of the buffer in bits. Defaults to -1 (unlimited).

    """

    def __init__(self, size: float = -1) -> None:
        self.bundles: Set[Bundle] = set()
        self.bundles_heap: List[Tuple[float, Bundle]] = []
        if size < 0:
            size = float("inf")
        self.size = self.free = size

    def contains(self, bundle: Bundle) -> bool:
        """Find out whether a given Bundle is contained in the buffer."""
        return bundle in self.bundles

    def add(self, bundle: Bundle) -> None:
        """Add the provided Bundle to the buffer."""
        assert not self.contains(bundle)
        self.bundles.add(bundle)
        heapq.heappush(self.bundles_heap, (bundle.deadline, bundle))
        self.free -= bundle.size
        assert self.free >= 0.0

    def remove(self, bundle: Bundle) -> None:
        """Remove a Bundle from the buffer."""
        self.bundles.remove(bundle)
        self.bundles_heap.remove((bundle.deadline, bundle))
        # Restore heap structure after
        heapq.heapify(self.bundles_heap)
        self.free += bundle.size

    def peek_edf(self) -> Optional[Bundle]:
        """Get the Bundle with the smallest deadline."""
        if not self.bundles_heap:
            return None
        _, bundle = self.bundles_heap[0]
        return bundle

    def pop_edf(self) -> Optional[Bundle]:
        """Remove the Bundle with the smallest deadline."""
        if not self.bundles_heap:
            return None
        _, bundle = heapq.heappop(self.bundles_heap)
        self.bundles.remove(bundle)
        self.free += bundle.size
        return bundle

    def __len__(self) -> int:
        """Get the amount of Bundles in the buffer."""
        # NOTE: This also allows statements like `if buf` to check for items.
        return len(self.bundles_heap)

    def __getitem__(self, key) -> Any:
        """Get an item by its index, ordered by deadline (earliest first)."""
        return self.bundles_heap[key][1]

    def __contains__(self, item: object) -> bool:
        """Find out whether a given Bundle is contained in the buffer."""
        return item in self.bundles
