# encoding: utf-8

"""Module providing a Node allowing to wait until a Bundle was scheduled."""

import asyncio

from . import SimNode


class SchedulingEventNode(SimNode):
    """A node class allowing to wait until new Bundles have been scheduled."""

    def __init__(self, node_id, buffer_size, event_dispatcher, **kwargs):
        super().__init__(
            node_id,
            buffer_size,
            event_dispatcher,
            **kwargs
        )
        self._event = asyncio.Event()

    def set_bundle_scheduled(self):
        """Trigger the 'Bundle scheduled' event to unblock waiting tasks."""
        self._event.set()
        # Construct a whole new event for the next successful scheduling, so
        # new calls to wait_until_new_bundle_scheduled wait again.
        # NOTE: By this, we cannot easily "query" the Event status.
        self._event = asyncio.Event()

    def get_event(self):
        """Obtain a reference to the internal asyncio.Event."""
        return self._event

    async def wait_until_new_bundle_scheduled(self):
        """Wait until a new Bundle has been scheduled."""
        await self._event.wait()

    def store_and_schedule(self, bundle, tx_node):
        """Add the provided Bundle to the buffer and schedule it."""
        super().store_and_schedule(bundle, tx_node)
        # if a get_bundles generator is waiting for new Bundles, unblock it
        self.set_bundle_scheduled()

    async def get_bundles(self, rx_node):
        """Asynchronously yield Bundles to be transmitted."""
        raise NotImplementedError()
        yield  # mark function as generator -> pylint: disable=unreachable
