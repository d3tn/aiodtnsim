# encoding: utf-8

"""Module providing a ONE-like implementation of Spray and Wait Routing."""

import asyncio
import math
import random

from .opportunistic_node import DropStrategy, OpportunisticNode


class ONESprayAndWaitNode(OpportunisticNode):
    """A node class using binary Spray and Wait Routing as implemented in ONE.

    Args:
        node_id: A unique identifier for the node.
        buffer_size: The amount of data (in bits) the node's buffer can hold.
        event_dispatcher: An EventDispatcher for monitoring the simulation.
        link_factory: A callable returning a ``Link`` instance for the given
            parameters. If not provided, ``Link`` will be used.
        initial_copies (int): The initial number of copies for any Bundle.

    """

    def __init__(self, node_id, buffer_size, event_dispatcher,
                 initial_copies=6, **kwargs):
        super().__init__(
            node_id,
            buffer_size,
            event_dispatcher,
            drop_strategy=DropStrategy.FIFO,  # first-received first
            **kwargs
        )
        self.initial_copies = initial_copies
        self._bdl_metadata = {}
        self.delivered = set()

    async def get_bundles(self, rx_node):
        """Asynchronously yield Bundles to be transmitted."""
        in_flight = None
        while True:
            deliverable = [
                bdl for bdl in self.buf
                if rx_node.node_id == bdl.destination
                # Optimization: Do not send Bundles the receiver has already.
                # NOTE: This is ONE-specific and not quite realistic.
                and bdl not in rx_node.buf
                and bdl not in rx_node.delivered
                and bdl is not in_flight
            ]
            population = [
                bdl for bdl in self.buf
                if self._bdl_metadata[bdl] > 1
                and rx_node.node_id != bdl.destination
                # Optimization: Do not send Bundles the receiver has already.
                # NOTE: This is ONE-specific and not quite realistic.
                and bdl not in rx_node.buf
                and bdl not in rx_node.delivered
                and bdl is not in_flight
            ]
            random.shuffle(population)
            random.shuffle(deliverable)
            event = self.get_event()
            while not event.is_set():
                # Unfortunately, we cannot yield > 1 Bundle as the population
                # may change during transmission which occurs during `yield`.
                if deliverable:
                    # First, send Bundles for which neighbor == receiver.
                    # This behavior has been adopted from the ONE simulator.
                    used_list = deliverable
                elif population:
                    # Second, send other Bundles for which we have copies.
                    used_list = population
                else:
                    # Nothing to send, wait for a new Bundle.
                    in_flight = None
                    await self.wait_until_new_bundle_scheduled()
                    break
                # Send ONE Bundle.
                bundle = used_list.pop()
                # It might happen that a Bundle is removed from the buffer
                # but nothing new gets scheduled, so we don't need a new
                # population but have to check whether we still have it.
                if not self.buf.contains(bundle):
                    continue
                if bundle.deadline < asyncio.get_running_loop().time():
                    self.event_dispatcher.bundle_dropped(self, bundle)
                    self.buf.remove(bundle)
                    continue
                copies = self._bdl_metadata[bundle]
                if copies <= 1:
                    if rx_node.node_id != bundle.destination:
                        continue
                    assert rx_node.node_id == bundle.destination
                    out_copies = 1
                    keep_copies = 1  # NOTE: ONE-specific!
                else:
                    # See the Spray and Wait paper, definition 3.2.
                    out_copies = math.floor(copies / 2)
                    keep_copies = math.ceil(copies / 2)
                # We now have given copies away
                self._bdl_metadata[bundle] = keep_copies
                # Yield the Bundle and transmit the copy count as metadata.
                in_flight = bundle
                try:
                    yield bundle, out_copies
                except GeneratorExit:
                    # Transmission failed, add back the failed copies.
                    self._bdl_metadata[bundle] += out_copies
                    raise
                # If the Bundle was not sent out successfully, we will not
                # get here but yield will throw.
                if keep_copies == 0 and bundle in self.buf:
                    self.event_dispatcher.bundle_deleted(self, bundle)
                    self.buf.remove(bundle)

    def route(self, bundle, tx_node, metadata):
        """Schedule the provided Bundle for reception at the next hop."""
        if tx_node:
            # NOTE: We cannot really ensure we do not get Bundles we already
            # have. If a node chooses to send something to us we have to
            # increase the number of copies we have by the forwarded number
            # of copies.
            if self.node_id == bundle.destination:
                self.delivered.add(bundle)
            else:
                if bundle not in self._bdl_metadata:
                    self._bdl_metadata[bundle] = 0
                self._bdl_metadata[bundle] += metadata
        else:
            self._bdl_metadata[bundle] = self.initial_copies  # injected
        super().route(bundle, tx_node, metadata)
