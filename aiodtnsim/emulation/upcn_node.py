# encoding: utf-8

"""A Node class allowing integration of the uPCN DTN software."""

import asyncio
import subprocess
import logging
import time
import os
import atexit

from typing import Any, Optional, Set, Dict

from pyupcn.agents import ConfigMessage, Contact as ContactDef
from pyupcn.helpers import UNIX_EPOCH, unix2dtn
from pyupcn import bundle7

from .. import BaseNode, Bundle, Contact
from ..timeutil import sleep_until, duration_until
from .aio_upcn_interface import AsyncTCPSPPConnection

KEEPALIVE_TIMEOUT = 300

logger = logging.getLogger(__name__)


class UPCNNode(BaseNode):
    """A Node implementation allowing to integrate the uPCN DTN software.

    Args:
        node_id: The DTN node identifier for the uPCN instance.
        event_dispatcher: An EventDispatcher for monitoring the simulation.
        upcn_path: The absolute path to the uPCN binary to be executed.
        spp_port: The port to be used for SPP.
        aap_port: The port to be used for AAP.
        reachable_nodes: A dict associating all neighbors of the Node to EIDs
            reachable via them. Will be provided to uPCN.
            The format is equal to that of the `vertices` dict contained in
            a TVG representation obtained from the `tvgutil` package.
        contact_setup_time: The time, in seconds, allowed before the contact
            start for uPCN to setup the connection.
            NOTE: When setting the setup time to zero we will
            run into synchronization issues (i.e. a race condition where the
            contact in uPCN starts earlier, leading to dropped bundles).
        contact_min_duration: The minimum duration, in seconds, for a contact
            to be used in uPCN. This prevents scheduling contacts that are
            too short in schedule_contact.
        max_tx_channels: The maximum amount of simultaneous transmission
            channels to be allocated by this node. If this amount of channels
            is reached, no new outgoing connections can be established.
            The default is None, meaning an unlimited amount.
        loop: The asyncio EventLoop to be used.

    """

    def __init__(self, node_id: str, event_dispatcher: Any,
                 upcn_path: str, spp_port: int, aap_port: int,
                 reachable_nodes: Dict[str, Set[str]],
                 contact_setup_time: float = 1.,
                 contact_min_duration: int = 1,
                 max_tx_channels: Optional[int] = None,
                 max_rx_channels: Optional[int] = None,
                 aap_lifetime: int = 86400,
                 loop: asyncio.AbstractEventLoop = None) -> None:
        super().__init__(
            node_id,
            event_dispatcher,
            tx_channels=max_tx_channels,
            rx_channels=max_rx_channels,
        )
        self.reachable_nodes = reachable_nodes
        self.contact_setup_time = contact_setup_time
        self.contact_min_duration = contact_min_duration
        self.receiving_from: Set[str] = set()
        self.current_contacts: Dict[str, Contact] = dict()
        self.tx_queue: asyncio.Queue = asyncio.Queue()
        upcn_process = subprocess.Popen([
            upcn_path,
            "-c",
            f"tcpspp:*,{spp_port}",
            "-a",
            str(aap_port),
            "-e",
            node_id,
            "-l",
            str(aap_lifetime),
        ])
        atexit.register(upcn_process.terminate)
        logger.info(
            "Spawned uPCN (%d) for ID '%s': SPP @ %d/tcp, AAP @ %d/tcp",
            upcn_process.pid,
            node_id,
            spp_port,
            aap_port,
        )
        loop = loop or asyncio.get_event_loop()
        self.time_offset_unix = int(time.time() - loop.time())
        # We need to keep track of which bundles came from other nodes and
        # which came in via AAP.
        self.known_bundles: Set[Bundle] = set()
        asyncio.ensure_future(
            self._upcn_handler("localhost", spp_port),
            loop=loop,
        )

    def add_known_bundle(self, bundle):
        """Add a Bundle to the set of known bundles.

        If a Bundle is not contained in this set, an injection event is
        triggered upon reception. This could be the case, e.g., if the
        Bundle was received via AAP.

        """
        original = bundle.original_bundle
        if original not in self.known_bundles:
            self.known_bundles.add(original)

    async def schedule_contact(self, contact: Contact) -> None:
        """Schedule the provided contact for execution."""
        # NOTE: This schedules uPCN == TX contacts.
        #       Contacts where uPCN == RX are opportunistic.
        assert self is contact.tx_node
        # Prevent scheduling contacts that are too short
        start_time = unix2dtn(self._sim2unix(contact.start_time))
        end_time = unix2dtn(self._sim2unix(contact.end_time))
        if (end_time <= start_time or
                end_time - start_time < self.contact_min_duration):
            logger.debug("Contact too short, dropping: %s", contact)
            return
        # Send the scheduling command to uPCN
        await self._schedule_contact_in_upcn(contact, start_time, end_time)
        # Wait until the contact starts
        await sleep_until(contact.start_time - self.contact_setup_time)
        # Add to local distribution list
        assert contact.rx_node.node_id not in self.current_contacts
        self.current_contacts[contact.rx_node.node_id] = contact
        self.event_dispatcher.contact_started(contact)
        # Wait until the contact has _really_ started.
        await sleep_until(contact.start_time)
        # Wait until the contact ends
        await sleep_until(contact.end_time)
        # Remove from local distribution list
        del self.current_contacts[contact.rx_node.node_id]
        self.event_dispatcher.contact_ended(contact)

    async def _schedule_contact_in_upcn(self, contact: Contact,
                                        start_time: int,
                                        end_time: int) -> None:
        bit_rate = contact.bit_rate // 8  # uPCN bit rate is in bytes/sec
        config_bundle = bundle7.create_bundle7(
            "dtn:manager",
            self.node_id + "/config",
            bytes(ConfigMessage(
                contact.rx_node.node_id,
                "tcpspp:out",
                contacts=[
                    ContactDef(
                        start=int(start_time),
                        end=int(end_time),
                        bitrate=int(bit_rate),
                    )
                ],
                reachable_eids=list(
                    self.reachable_nodes.get(contact.rx_node.node_id, [])
                ),
            )),
        )
        await self.tx_queue.put(bytes(config_bundle))

    async def schedule_injection(self, bundle: Bundle) -> None:
        """Schedule the provided Bundle for injection into the network."""
        await sleep_until(bundle.start_time)
        self.add_known_bundle(bundle)
        self.event_dispatcher.bundle_injected(self, bundle)
        await self._bundle_to_upcn(bundle)

    async def schedule_reception(self, bundle: Bundle, tx_node: BaseNode,
                                 delay: float, metadata: Any) -> None:
        """Schedule the provided Bundle for reception at the next hop."""
        await asyncio.sleep(delay)
        self.add_known_bundle(bundle)
        self.event_dispatcher.bundle_received(self, bundle, tx_node)
        await self._bundle_to_upcn(bundle)

    async def _bundle_to_upcn(self, bundle: Any):
        if self.node_id == bundle.destination:
            self.event_dispatcher.bundle_delivered(self, bundle)
        try:
            binary_data = bundle.data
            if binary_data is None:
                raise TypeError
        except (AttributeError, TypeError):
            creation_timestamp = self._sim2unix(bundle.start_time)
            lifetime = self._sim2unix(bundle.deadline) - creation_timestamp
            size_bytes = bundle.size // 8
            assert size_bytes * 8 == int(bundle.size)
            bundle = bundle7.create_bundle7(
                bundle.source,
                bundle.destination,
                os.urandom(size_bytes),
                creation_timestamp=creation_timestamp,
                lifetime=lifetime,
            )
            binary_data = bytes(bundle)
            # reduce bundle size by header overhead to get approx. the
            # expected bundle size on wire - NOTE: only _approximately_!
            header_overhead = len(binary_data) - size_bytes
            new_pl_len = size_bytes - header_overhead
            assert new_pl_len > 0
            bundle.payload_block.data = bundle.payload_block.data[:new_pl_len]
            binary_data = bytes(bundle)
        logger.debug(
            "Sending bundle to '%s' (size = %d) to uPCN (ID '%s')",
            bundle.destination,
            len(binary_data),
            self.node_id,
        )
        await self.tx_queue.put(binary_data)

    async def _upcn_handler(self, host: str, spp_port: int) -> None:
        spp_connection = AsyncTCPSPPConnection(host, spp_port)
        async with spp_connection:
            logger.info(
                "Connected via TCPSPP to %s:%d/tcp",
                host,
                spp_port,
            )
            await asyncio.wait({
                asyncio.create_task(self._handle_upcn_rx(spp_connection)),
                asyncio.create_task(self._handle_upcn_tx(spp_connection)),
            })

    async def _handle_upcn_rx(self, spp_connection: AsyncTCPSPPConnection):
        while True:
            spp_pl = await spp_connection.recv_spp()
            await self._bundle_from_upcn(spp_pl)

    async def _handle_upcn_tx(self, spp_connection: AsyncTCPSPPConnection):
        while True:
            try:
                outgoing = await asyncio.wait_for(
                    self.tx_queue.get(),
                    timeout=KEEPALIVE_TIMEOUT,
                )
                assert isinstance(outgoing, bytes)
                await spp_connection.send_spp(outgoing)
            except asyncio.TimeoutError:
                # Send a NULL-byte for TCP keepalive purposes
                await spp_connection.send_spp(b"\0")

    async def _bundle_from_upcn(self, spp_payload: bytes) -> None:
        bundle = bundle7.Bundle.parse(spp_payload)
        logger.debug(
            "Bundle received from uPCN @ '%s': '%s' -> '%s'",
            self.node_id,
            bundle.primary_block.source,
            bundle.primary_block.destination,
        )
        # Bundle properties
        creation_timestamp_unix = (
            bundle.primary_block.creation_time.time - UNIX_EPOCH
        ).total_seconds()
        start_time = self._unix2sim(creation_timestamp_unix)
        deadline = self._unix2sim(
            creation_timestamp_unix +
            bundle.primary_block.lifetime
        )
        # NOTE: We create a new Bundle all the time! (the Bundle changes)
        bundle = Bundle(
            start_time=start_time,
            # NOTE: We need to pass a NODE ID, but uPCN's EIDs should always
            # also be node ids.
            source=_base_eid(bundle.primary_block.source),
            destination=_base_eid(bundle.primary_block.destination),
            # Bundle contains the size of the serialized bundle in bits
            size=(len(spp_payload) * 8),
            deadline=deadline,
            fragment_offset=(bundle.primary_block.fragment_offset
                             if bundle.is_fragmented else None),
            total_size=(bundle.primary_block.total_payload_length * 8
                        if bundle.is_fragmented else None),
            data=spp_payload,
        )
        # XXX Bundles may be injected manually, e.g. via AAP. As these are
        # unknown to the simulator, an injection event has to be triggered
        # when they are first received from uPCN.
        if bundle.original_bundle not in self.known_bundles:
            self.event_dispatcher.bundle_injected(self, bundle)
        # Simple broadcast transmission, waiting until Bundles arrive
        # This waits until the Bundle has been transmitted via all contacts.
        await asyncio.gather(*[
            self._schedule_for_contact(bundle, contact)
            for _, contact in self.current_contacts.items()
        ])

    async def _schedule_for_contact(self, bundle: Bundle,
                                    contact: Contact) -> None:
        assert self is contact.tx_node
        try:
            await asyncio.wait_for(
                self._send_via_contact(bundle, contact),
                timeout=duration_until(contact.end_time),
            )
        except asyncio.TimeoutError:
            pass

    async def _send_via_contact(self, bundle: Bundle,
                                contact: Contact) -> None:
        async with self.channel(contact.rx_node):
            self.event_dispatcher.bundle_transmission_started(
                bundle,
                contact,
            )
            tx_duration = bundle.size / contact.bit_rate
            try:
                await asyncio.sleep(tx_duration)
            except asyncio.CancelledError:
                self.event_dispatcher.bundle_transmission_aborted(
                    bundle,
                    contact,
                )
                raise
            self.event_dispatcher.bundle_transmission_completed(
                bundle,
                contact,
            )
        # We schedule reception of the Bundle after the specified link delay.
        asyncio.ensure_future(contact.rx_node.schedule_reception(
            bundle,
            self,
            contact.delay,
            None,  # NOTE: Should we fwd. metadata for e.g. SnW?
        ))

    def _sim2unix(self, sim_time: float):
        return int(sim_time + self.time_offset_unix)

    def _unix2sim(self, dtn_time: int):
        return int(dtn_time - self.time_offset_unix)


def _base_eid(eid: bundle7.EID):
    base_eid = "/".join(str(eid).split("/")[:-1])
    # The latter case indicates we have split a "dtn://"-EID without agent ID.
    if base_eid in ("", "dtn:/"):
        return str(eid)
    return base_eid
