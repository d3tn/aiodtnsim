# encoding: utf-8

"""A discrete-event simulator for delay-tolerant networks, based on asyncio."""

from typing import (
    Any,
    AsyncGenerator,
    Optional,
)

import asyncio
import contextlib
import functools
import dataclasses


@dataclasses.dataclass(frozen=True, order=True)
class Contact:
    """Represents a unidirectional factual contact.

    This directly corresponds to a time frame during which a link between
    two nodes is up, plus the associated properties.

    Args:
        tx_node: A Node instance representing the sending side of the link.
        rx_node: A Node instance representing the receiving side of the link.
        start_time: The time at which the link becomes available.
        end_time: The time at which the link will become unavailable.
        bit_rate: The transmission rate of the link, in bit/s.
        delay: The time it takes for a signal to reach the receiving node,
            in seconds.
        param: Arbitrary parameters for use by custom implementations.

    """

    tx_node: "BaseNode"
    rx_node: "BaseNode"
    start_time: float
    end_time: float
    bit_rate: float
    delay: float = 0.0
    param: Any = dataclasses.field(default=None, compare=False)


@dataclasses.dataclass(frozen=True, order=True)
class Bundle:
    """Represents a DTN Bundle.

    Args:
        start_time: The time at which the Bundle is injected/created.
        source: The unique identifier (node ID) of the source node.
        destination: The unique identifier (node ID) of the destination node.
        size: The Bundle size in bits.
        deadline: The time at which the Bundle expires, i.e. a time after
            which forwarding of the Bundle is not sensible anymore.
        fragment_offset: For fragmented Bundles, this contains the fragment
            offset in bits from the start of the original Bundle.
        total_size: For fragmented Bundles, this contains the total
            length of the payload of the original Bundle.
        data: An optional payload data field to hand over Bundle contents.

    """

    start_time: float
    source: str
    destination: str = dataclasses.field(compare=False)
    size: int = dataclasses.field(compare=False)
    deadline: float = dataclasses.field(compare=False)
    fragment_offset: Optional[int] = None
    total_size: Optional[int] = None
    data: Any = dataclasses.field(default=None, compare=False, repr=False)

    @property
    def is_fragmented(self):
        """Get whether the Bundle is a fragment of another Bundle."""
        return self.fragment_offset is not None

    @property
    def original_bundle(self):
        """Get a Bundle equal to the original Bundle of this fragment."""
        if self.is_fragmented:
            return Bundle(
                start_time=self.start_time,
                source=self.source,
                destination=self.destination,
                size=self.total_size,
                deadline=self.deadline,
            )
        return self


class BaseNode:
    """Base class representing a DTN node.

    Args:
        node_id: A unique identifier for the node.
        event_dispatcher: An EventDispatcher for monitoring the simulation.

    """

    def __init__(self, node_id: str, event_dispatcher: Any,
                 tx_channels: Optional[int] = None,
                 rx_channels: Optional[int] = None,
                 link_rx_tx_channels: bool = False) -> None:
        self.node_id = node_id
        self.event_dispatcher = event_dispatcher
        self.tx_channel_sem: Optional[asyncio.BoundedSemaphore] = (
            asyncio.BoundedSemaphore(tx_channels)
            if tx_channels else None
        )
        if link_rx_tx_channels:
            assert tx_channels == rx_channels
        self.rx_channel_sem: Optional[asyncio.BoundedSemaphore] = (
            self.tx_channel_sem if link_rx_tx_channels
            else (
                asyncio.BoundedSemaphore(rx_channels) if rx_channels
                else None
            )
        )

    @contextlib.asynccontextmanager
    async def channel(self, rx_node: "BaseNode") -> AsyncGenerator:
        """Reserve a tx_channel at this node and an rx_channel at rx_node."""
        # This logic allows to prevent unnecessary blocking of TX/RX channels.
        # If one of them is not available, the other will not be blocked,
        # until both are available at the same time.
        while True:
            # First, wait for a TX channel, then, an RX channel.
            async with self.tx_channel(rx_node):
                # If we cannot obtain an RX channel _right now_, try it the
                # other way around (see below).
                if (rx_node.rx_channel_sem is None or
                        not rx_node.rx_channel_sem.locked()):
                    async with rx_node.rx_channel(self):
                        # Got both, allow transmissions!
                        yield
                        break
            # Try it the other way around...
            async with rx_node.rx_channel(self):
                if (self.tx_channel_sem is None or
                        not self.tx_channel_sem.locked()):
                    async with self.tx_channel(rx_node):
                        # Got both, allow transmissions!
                        yield
                        break

    @contextlib.asynccontextmanager
    async def rx_channel(self, tx_node: "BaseNode") -> AsyncGenerator:
        """Reserve a free channel to receive data."""
        if not self.rx_channel_sem:
            yield
            return
        await self.rx_channel_sem.acquire()
        try:
            yield
        finally:
            self.rx_channel_sem.release()

    @contextlib.asynccontextmanager
    async def tx_channel(self, rx_node: "BaseNode") -> AsyncGenerator:
        """Reserve a free channel to transmit data."""
        if not self.tx_channel_sem:
            yield
            return
        await self.tx_channel_sem.acquire()
        try:
            yield
        finally:
            self.tx_channel_sem.release()

    async def schedule_contact(self, contact: Contact) -> None:
        """Schedule the provided contact for execution.

        This coroutine has to terminate after the link has been deestablished.

        """
        raise NotImplementedError()

    async def schedule_injection(self, bundle: Bundle) -> None:
        """Schedule the provided Bundle for injection into the network.

        This coroutine has to terminate after the Bundle has been routed.

        """
        raise NotImplementedError()

    async def schedule_reception(self, bundle: Bundle, tx_node: "BaseNode",
                                 delay: float, metadata: Any) -> None:
        """Schedule the provided Bundle for reception at the next hop.

        This coroutine has to terminate after the Bundle has been routed.

        """
        raise NotImplementedError()

    def __repr__(self) -> str:
        """Get a string representation of the class instance."""
        return f"<BaseNode node_id='{self.node_id}'>"


class EventHandler:
    """Defines the interface for classes handling simulator events."""

    def contact_started(self, time: float, contact: Contact) -> None:
        """Event triggered when a contact has started."""

    def contact_ended(self, time: float, contact: Contact) -> None:
        """Event triggered when a contact has ended."""

    def bundle_received(self, time: float, rx_node: BaseNode,
                        bundle: Bundle, tx_node: BaseNode) -> None:
        """Event triggered when a bundle has been received."""

    def bundle_dropped(self, time: float, node: BaseNode,
                       bundle: Bundle) -> None:
        """Event triggered when a bundle has been dropped from the buffer."""

    def bundle_rejected(self, time: float, node: BaseNode,
                        bundle: Bundle) -> None:
        """Event triggered when a Bundle has been dropped on reception."""

    def bundle_delivered(self, time: float, node: BaseNode,
                         bundle: Bundle) -> None:
        """Event triggered when a Bundle has been delivered successfully."""

    def bundle_deleted(self, time: float, node: BaseNode,
                       bundle: Bundle) -> None:
        """Event triggered when a Bundle has been dropped after TX."""

    def bundle_injected(self, time: float, node: BaseNode,
                        bundle: Bundle) -> None:
        """Event triggered when a Bundle has been injected/created."""

    def bundle_scheduled(self, time: float, node: BaseNode,
                         bundle: Bundle) -> None:
        """Event triggered when a Bundle has been stored and scheduled."""

    def bundle_transmission_started(self, time: float, bundle: Bundle,
                                    contact: Contact) -> None:
        """Event triggered when a Bundle transmission is initiated."""

    def bundle_transmission_completed(self, time: float, bundle: Bundle,
                                      contact: Contact) -> None:
        """Event triggered when a Bundle transmission succeeded."""

    def bundle_transmission_aborted(self, time: float, bundle: Bundle,
                                    contact: Contact) -> None:
        """Event triggered when a Bundle transfer has been aborted."""


class EventDispatcher:
    """Provides an extensible monitoring interface for the simulator."""

    def __init__(self):
        self._handlers = {
            event: []
            for event in EventHandler.__dict__ if not event.startswith("__")
        }

        def _dispatch_event(callbacks, *args, **kwargs):
            if not callbacks:
                return
            loop = asyncio.get_running_loop()
            for func in callbacks:
                func(loop.time(), *args, **kwargs)

        # This adds a method for each event to the current instance.
        for event_name, callbacks in self._handlers.items():
            setattr(self, event_name, functools.partial(
                _dispatch_event,
                callbacks,  # NOTE that callbacks is a [] reference
            ))

    def add_subscriber(self, subscriber: EventHandler, no_parent: bool = True):
        """Add an event subscriber to be notified.

        Args:
            subscriber: An EventHandler instance to be notified.
            no_parent: Do not add non-overridden methods of parent
                classes for efficiency. Defaults to True.

        """
        for event, handler_list in self._handlers.items():
            register_method = (
                (event in subscriber.__class__.__dict__) if no_parent
                else hasattr(subscriber, event)
            )
            if register_method:
                handler_list.append(getattr(subscriber, event))
